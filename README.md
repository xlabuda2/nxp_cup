# NXP_CUP

## TODO

### HW
 - [ ] Display na debug (confing, camera view)
 - [ ] Controller na jazdenie + killswitch
 - [ ] Laser na point kde sa diva kamera
 - [ ] Normalna kamera
 - [ ] Linear 2x
 - [ ] Linear attached on servo
 - [ ] Vlastna doska (zmesti sa na podvozok, velky vykon)
 - [ ] Mensie baterky (zmesti sa na podvozok)
 - [ ] Akcelerometer pre zistenie presmykovania (control loop rychlosit)
 - [ ] Vlastna linecam
 - [ ] Nakupit linecam do zasoby
 - [ ] Repracik
 - [ ] Lepsi distance sensor
 - [ ] Slunicko
 - [ ] PiXy firmware
 - [ ] Battery box that won't screw batteries

 ### SW
 - [ ] Logovat vela dat na debugovanie (realtime)
 - [ ] Refaktor a kontrola algoritmu
 - [ ] Zastavenie ak vybehneme z drahy
 - [ ] Pouzit DMA pre zrychlenie
 - [ ] Spracovanie dat z normalnej kamery?
 - [ ] Loopback na rychlost nech rata s presmykom
 - [ ] Event handler

### Car
 - [ ] 4x4 podvozok
 - [ ] Lepsie pneumatiky (gumenne)
 - [ ] Skontrolovat nastavenie podvozku (antiaccerman?)
 - [ ] Smetak na bordel
 - [ ] Zavazicka a posunut cele taziste dole
 - [ ] Vrtulka na podtlak 
 - [ ] Tensia tycka / stojan na kameru (karbon)
 - [ ] Vyhrievanie koliesok
 - [ ] Spoiler a aerodinamika
 - [ ] Model karoserie / krytu
 - [ ] MCU expresso remote gdb (flashing) (check USB over network project)

### Wiki
 - [ ] Auticko / hw
 - [ ] MCU
 - [ ] Zdrojak

### Other
 - [ ] Naplanovat udrzatelny a pekny format pre repo / repa (protokoly, zdrojaky, dokumentacia, etc.)
 - [ ] Naplanovat architekturu / periferie pred vymyslanim dosky
 - [ ] Riesenie + testovanie flickering osvetlenia
 - [ ] Kupit lepsiu drahu (vlastna draha)
 - [ ] Testovacie priestory (rezervacia miestnosti pre seminar)
 - [ ] Driftovanie do zakrut?
 - [ ] Wireless connection (nastavenie configu, wireless programming)
 - [ ] Zvysit robustnost
 - [ ] Zvazit minipodvozok (micerace, elektro diff)
 - [ ] Simulacia / testovaci dataset na edgecase (tiene)
 - [ ] Zakomponovat hybrid pixy / linecam
 - [ ] Testovat horsie podmienky
 - [ ] Viac deadlinov
 - [ ] Co je povolene?
 - [ ] Electromaker priebezne
 - [ ] Electromaker checknut ostatne teamy
 - [ ] Youtube contest?
 - [ ] Spolupraca so slovenskou skolou
 - [ ] Skusit zohnat miestnost na seminar
 - [ ] Mantinely (pool noodles)

## Ideas
 - [ ] [Doska na zbieranie dat](https://www.laskakit.cz/radxa-zero-d1-1gb-ram-gpio-header-antenna/)

## Links
 - [Navod k reglu](https://docplayer.cz/11069270-Xc500tf-xc550tf-xc550tf-mj-modelu-aut.html)